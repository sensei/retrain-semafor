#!/bin/bash

. config.sh

# bring in needed files
mkdir -p lrdata
ln -sf ${SEMAFOR_HOME}/file_properties.xml .
ln -sf ${SEMAFOR_HOME}/dict .
ln -sf ${SEMAFOR_HOME}/stopwords.txt lrdata
mkdir -p ${datadir}/scan

# step 1: Alphabet Creation
$jhome/java -classpath ${classpath} -Xms4000m -Xmx4000m edu.cmu.cs.lti.ark.fn.parsing.CreateAlphabet \
    ${fefile}.frame.elements \
    ${fefile}.all.lemma.tags \
    ${datadir}/scan/cv.train.events.bin \
    ${datadir}/scan/parser.conf.unlabeled \
    ${datadir}/scan/cv.train.sentences.frame.elements.spans \
    true \
    false \
    1 \
    null \
    ${datadir}/framenet.frame.element.map 


# step 2: Caching Feature Vectors
$jhome/java -classpath ${classpath} -Xms4000m -Xmx4000m edu.cmu.cs.lti.ark.fn.parsing.FrameFeaturesCache \
    eventsfile:${datadir}/scan/cv.train.events.bin \
    spansfile:${datadir}/scan/cv.train.sentences.frame.elements.spans \
    train-framefile:${fefile}.frame.elements \
    localfeaturescache:${datadir}/scan/featurecache.jobj

# step 3: training
$jhome/java -classpath ${classpath} -Xms8000m -Xmx8000m edu.cmu.cs.lti.ark.fn.parsing.TrainingBatchMain \
    model:${datadir}/argmodel.dat \
    alphabetfile:${datadir}/scan/parser.conf.unlabeled \
    localfeaturescache:${datadir}/scan/featurecache.jobj \
    train-framefile:${fefile}.frame.elements \
    regularization:reg \
    lambda:0.1 \
    numthreads:4 \
    binaryoverlapfactor:false

model=`ls ${datadir}/argmodel.dat_* | sort -n -k3 -t_ | tail -1`
cp $model ${datadir}/argmodel.dat
