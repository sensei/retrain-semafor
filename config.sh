#!/bin/bash

# make bash exit as soon as a command fails
set -u -e -o pipefail



# === to be modified ===

# location of framenet data in its original xml format
framenet=/storage/raid1/corpora/fndata-1.5

# location of javac/java
jhome=/usr/bin/

# location of macaon common/config files
export MACAON_DIR=~benoit.favre/work/macaon/maca_data

# macaon parser model
parser_model=$MACAON_DIR/en/maca_graph_parser/maca_graph_parser_model1_en

# macaon tagger model
tagger_model=$MACAON_DIR/en/maca_crf_tagger/crf_tagger_model_en.bin
tagger_dictionary=$MACAON_DIR/en/maca_crf_tagger/crf_tagger_wordtag_lexicon_en.bin



# === keep the following as is ===

SEMAFOR_HOME=$PWD/deps/semafor-semantic-parser
classpath=${SEMAFOR_HOME}/lib/semafor-deps.jar:${SEMAFOR_HOME}
datadir=$PWD/data
luxmldir=$framenet/lu
fefile=${datadir}/framenet

