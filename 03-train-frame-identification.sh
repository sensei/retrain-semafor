#!/bin/bash

. config.sh

mkdir -p ${datadir}/events
rm -rf ${datadir}/log
end=`cat ${fefile}.frame.elements | wc -l`

# step 1: alphabet creation
$jhome/java -classpath ${classpath} -Xms8G -Xmx8G \
    edu.cmu.cs.lti.ark.fn.identification.AlphabetCreationThreaded \
    train-fefile:${fefile}.frame.elements \
    train-parsefile:${fefile}.all.lemma.tags \
    stopwords-file:${SEMAFOR_HOME}/stopwords.txt \
    wordnet-configfile:${SEMAFOR_HOME}/file_properties.xml \
    fnidreqdatafile:${datadir}/reqData.jobj \
    logoutputfile:${datadir}/log \
    model:${datadir}/alphabet.dat \
    eventsfile:${datadir}/events \
    startindex:0 \
    endindex:${end} \
    numthreads:4

# step 2: combine alphabets?
$jhome/java -classpath ${classpath} -Xms8G -Xms8G \
    edu.cmu.cs.lti.ark.fn.identification.CombineAlphabets \
    ${datadir} \
    ${datadir}/alphabet.dat

rm -rf ${datadir}/log
# step 3: creating feature events
$jhome/java -classpath ${classpath} -Xms8G -Xmx8G \
    edu.cmu.cs.lti.ark.fn.identification.CreateEventsUnsupported \
    train-fefile:${fefile}.frame.elements \
    train-parsefile:${fefile}.all.lemma.tags \
    stopwords-file:${SEMAFOR_HOME}/stopwords.txt \
    wordnet-configfile:${SEMAFOR_HOME}/file_properties.xml \
    fnidreqdatafile:${datadir}/reqData.jobj \
    logoutputfile:${datadir}/log \
    model:${datadir}/alphabet.dat \
    eventsfile:${datadir}/events \
    startindex:0 \
    endindex:${end} \
    numthreads:4

rm -rf ${datadir}/log
# step 4: traning the frame identification model
mkdir -p ${datadir}/models_0.0
$jhome/java -classpath ${classpath} -Xms8G -Xmx8G \
    edu.cmu.cs.lti.ark.fn.identification.TrainBatchModelDerThreaded \
    alphabetfile:${datadir}/alphabet.dat \
    eventsfile:${datadir}/events \
    model:${datadir}/models_0.0/idmodel.dat \
    regularization:reg \
    lambda:0.0 \
    restartfile:null \
    logoutputfile:${datadir}/log \
    numthreads:8

# step 5: convert alphabet files
model=`ls ${datadir}/models_0.0/idmodel.dat_* | sort -n -k3 -t_ | tail -1`
$jhome/java -classpath ${classpath} -Xms8G -Xms8G edu.cmu.cs.lti.ark.fn.identification.ConvertAlphabetFile \
    ${datadir}/alphabet.dat \
    $model \
    ${datadir}/idmodel.dat

