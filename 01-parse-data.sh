#!/bin/bash

. config.sh

# convert framenet examples to the format understood by semafor (documented in docs/data-format-readme.txt)
/usr/bin/env python scripts/fulltext_to_elements.py $framenet/fulltext/*.xml > $fefile.frame.elements

# convert framenet examples to the format understood by macaon
/usr/bin/env python scripts/fulltext_to_conll07.py $framenet/fulltext/*.xml > $fefile.conll07

# run macaon tagger+parser
maca_crf_barebones_decoder --conll07 $tagger_model $tagger_dictionary < $fefile.conll07 > $fefile.conll07.tagged
maca_graph_parser -M e -C en -c $fefile.conll07.tagged -m $parser_model.bin -a $parser_model.alpha -d $parser_model.dep_count -x -v 1 -z $fefile.conll07.parsed

# convert macaon output ot the format understood by semafor (documented in docs/data-format-readme.txt)
/usr/bin/env python scripts/conll07_to_tags.py < $fefile.conll07.parsed > $fefile.all.lemma.tags
