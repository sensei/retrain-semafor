#!/bin/bash

. config.sh

rm -rf deps/semafor 

# download semafor
wget https://semafor-semantic-parser.googlecode.com/files/SEMAFOR-2.1.tgz -O deps/SEMAFOR-2.1.tgz
tar -C deps -xf deps/SEMAFOR-2.1.tgz

# alternate source (note that some programs work differently):
#git clone https://github.com/sammthomson/semafor.git deps/semafor

# compile required classes
javac -cp ${classpath} $SEMAFOR_HOME/edu/cmu/cs/lti/ark/fn/{identification,parsing}/*.java

# make sure macaon is installed
which maca_crf_barebones_decoder
which maca_graph_parser

# make sure python-nltk is installed
python -c 'import nltk'
