import sys
from nltk.corpus import wordnet as wn

def process_sentence(words):
    output = [len(words)]
    output.extend([x[1] for x in words]) # word forms
    output.extend([x[3] for x in words]) # tags
    output.extend([x[7] for x in words]) # dependency labels
    output.extend([x[6] for x in words]) # dependency parent
    output.extend(['O' for x in words])    # dummy slot for NEs
    lemmas = [wn.morphy(x[1].lower().decode('utf-8')) for x in words]    # lemma from wordnet
    mapping = {'NN': 'n', 'NNS': 'n', 'JJ': 'a', 'JJS': 'a', 'JJR': 'a', 'RB': 'r', 'RBR': 'r', 'RBS': 'r', 'VB': 'v', 'VBD': 'v', 'VBN': 'v', 'VBG': 'v', 'VBP': 'v', 'VBZ': 'v'}
    for tokens in words:
        word = tokens[1].lower()
        tag = mapping[tokens[3]] if tokens[3] in mapping else ''
        if tokens[3] in mapping:
            lemma = wn.morphy(word.decode('utf-8'), mapping[tokens[3]])
        else:
            lemma = wn.morphy(word.decode('utf-8'))
        if lemma != None:
            output.append(lemma)
        else:
            output.append(word)
    print '\t'.join([str(x) for x in output])

lines = []
for line in sys.stdin:
    line = line.strip()
    if line == '':
        process_sentence(lines)
        lines = []
    else:
        lines.append(line.split())
