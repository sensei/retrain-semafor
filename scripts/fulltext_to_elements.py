import sys, re
import xml.etree.ElementTree as ET

ns = '{http://framenet.icsi.berkeley.edu}'

# find a namespace-prefixed xml element, optionally filtered by attrib name/value
def find(element, path, attrib = {}):
    path = re.sub(r'/([^/.])', '/' + ns + r'\1', path)
    output = []
    for node in element.findall(path):
        skip = False
        for name, value in attrib.items():
            if name not in node.attrib or (value != None and node.attrib[name] != value):
                skip = True
                break
        if not skip:
            output.append(node)
    return output

def find_first(element, path, attrib = {}):
    output = find(element, path, attrib)
    return output[0] if len(output) > 0 else None

def token_index(text):
    index = []
    token = 0
    in_separator = False
    for character in text:
        if character != ' ':
            if in_separator:
                in_separator = False
                token += 1
            index.append(token)
        else:
            index.append(-1)
            in_separator = True
    return index


sentence_id = 0

def process_fulltext_xml(filename):
    global sentence_id
    fp = open(filename)
    root = ET.parse(fp).getroot()
    fp.close()

    for sentence in find(root, './/sentence'):
        text = find_first(sentence, './text').text
        index = token_index(text)
        #print text
        tokens = text.strip().split()
        for annotation in find(sentence, ".//annotationSet[@status='MANUAL']"):
            if 'luName' in annotation.attrib:
                lexical_unit_name = annotation.attrib['luName']
                frame_name = annotation.attrib['frameName']

                target_label_node = find_first(annotation, ".//label", {'name': 'Target'})
                if target_label_node == None:
                    continue
                target_start, target_end = int(target_label_node.attrib['start']), int(target_label_node.attrib['end'])
                target = text[target_start: target_end + 1]
                while index[target_start] == -1:
                    target_start += 1
                while index[target_end] == -1:
                    target_end -=1
                target_token_number = '_'.join([str(x) for x in range(index[target_start], index[target_end] + 1)])

                #print '  ', frame_name, lexical_unit_name, target, target_token_number
                frame_elements = []
                for frame_element_node in find(annotation, "./layer[@name='FE']/label[@start]"):
                    frame_element = frame_element_node.attrib['name']
                    frame_element_start, frame_element_end = int(frame_element_node.attrib['start']), int(frame_element_node.attrib['end'])
                    #print '     ', frame_element, index[frame_element_start], index[frame_element_end], text[frame_element_start: frame_element_end]
                    while index[frame_element_start] == -1:
                        frame_element_start += 1
                    while index[frame_element_end] == -1:
                        frame_element_end -= 1
                    frame_elements.append(frame_element)
                    frame_elements.append('%d:%d' % (index[frame_element_start], index[frame_element_end]))

                output = [len(frame_elements) / 2 + 1, frame_name, lexical_unit_name, target_token_number, target, sentence_id]
                output.extend(frame_elements)
                print '\t'.join([str(x) for x in output])
        sentence_id += 1

if __name__ == '__main__':
    for filename in sorted(sys.argv[1:]):
        print >>sys.stderr, filename
        process_fulltext_xml(filename)
