#!/bin/bash

MACAON_DIR=$HOME/work/macaon/macaon-for-uw_20140710/models/common/ 
MODEL=$MACAON_DIR/../parser/ptb-order1/maca_graph_parser_model1_en
TAGGER=$HOME/work/macaon/git-maca_data/en/maca_crf_tagger/

input=$1

maca_crf_barebones_decoder --conll07 $TAGGER/crf_tagger_model_en.bin $TAGGER/crf_tagger_wordtag_lexicon_en.bin < $input > $input.tagged
maca_graph_parser -M e -C en -c $input.parsed -m $MODEL.bin -a $MODEL.alpha -d $MODEL.dep_count -x -v 1 -z $input.parsed
